#-*- coding: utf-8 -*-
__author__ = 'Evgeniy Tatarintsev'
from StringIO import StringIO
import gzip

from grabbers.models import Entry


class Parser(object):
    def __init__(self, domain):
        self.domain = domain
        self._items = []

    def create_item(self, title=None, description=None, content=None,
                     image_url=None, posted=None, link=None):
        self._items.append(dict(title=title,
                                description=description,
                                content=content,
                                image_url=image_url,
                                posted=posted,
                                link=link))

    def items(self):
        return self._items

    def save(self):
        for item in self.items():
            image_url = item['image_url']
            del item['image_url']
            Entry.objects.create(domain=self.domain, **item)

    def run(self, date_from, parsed_link=[]):
        raise NotImplementedError


#Пул парсеров, содержит объекты класса Parser
parsers_pool = []


def get_page_content(page):
    if page.info().get('Content-Encoding') == 'gzip':
        buf = StringIO(page.read())
        f = gzip.GzipFile(fileobj=buf)
        content = f.read()
    else:
        content = page.read()

    return content

def register(parser):
    if isinstance(parser, Parser):
        parsers_pool.append(parser)
    else:
        raise TypeError


from grabbers.catalog import *