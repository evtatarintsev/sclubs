__author__ = 'Evgeniy Tatarintsev'
from django.contrib import admin
from django import forms
from django.utils.translation import gettext_lazy as _
from ckeditor.widgets import CKEditorWidget

from sclubs.news.models import Entry as NewsEntry
from models import Entry


class EntryAdminForm(forms.ModelForm):
    class Meta:
        widgets = {
            'content': CKEditorWidget(),
        }


class EntryAdmin(admin.ModelAdmin):
    form = EntryAdminForm
    readonly_fields = ('domain', 'link', 'posted')
    list_filter = ('domain',)
    list_display = ('title', 'domain', 'posted', 'moved_to_news')
    search_fields = ('title', 'domain', 'description', 'content')

    actions = ('move_to_news', )

    def move_to_news(self, request, queryset):
        grabbed_news = queryset.filter(moved_to_news=False)
        for entry in grabbed_news:
            NewsEntry.objects.create(title=entry.title,
                                     meta_description=entry.description,
                                     user=request.user,
                                     publish_date=entry.posted,
                                     content=entry.content,
                                     source=entry.link,)
        grabbed_news.update(moved_to_news=True)

    move_to_news.short_description = _('Move to news')


admin.site.register(Entry, EntryAdmin)