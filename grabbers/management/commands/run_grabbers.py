__author__ = 'Evgeniy Tatarintsev'
import datetime
from django.core.management import CommandError, BaseCommand

import grabbers


class Command(BaseCommand):
    def handle(self, *args, **options):
        parsers = grabbers.parsers_pool
        if len(args) > 1:
            parsers = []
            for parser in grabbers.parsers_pool:
                if parser.domain in args[1:]:
                    parsers.append(parser)

        for parser in parsers:
            entry_list = grabbers.Entry.objects.filter(domain=parser.domain)
            if entry_list:
                date_from = entry_list.latest('posted').posted
            else:
                date_from = datetime.datetime.date(datetime.datetime.today()-datetime.timedelta(days=2))

            parsed_link = entry_list.filter(posted=date_from).values_list('link')

            parser.run(date_from, parsed_link=parsed_link)
            parser.save()



