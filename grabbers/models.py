__author__ = 'Evgeniy Tatarintsev'
from django.db import models
from django.utils.translation import gettext_lazy as _


class Entry(models.Model):
    title = models.CharField(_('Title'), max_length=255)
    image = models.ImageField(_('Image'), upload_to='grabbers', null=True, blank=True)
    description = models.TextField(_('Description'), null=True, blank=True)
    content = models.TextField(_('Content'))
    posted = models.DateField(_('Posted date'))

    moved_to_news = models.BooleanField(_('Moved to news'), default=False)
    domain = models.URLField(_('Source site'))
    link = models.URLField(_('Entry url'), unique=True)

    class Meta:
        verbose_name = _('Entry')
        verbose_name_plural = _('Entries')

    def __unicode__(self):
        return self.title