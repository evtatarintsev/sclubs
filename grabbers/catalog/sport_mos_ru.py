#-*- coding: utf-8 -*-
__author__ = 'Evgeniy Tatarintsev'
import urllib2
import datetime
from bs4 import BeautifulSoup

import grabbers

HTTP_HEADERS = {
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:25.0) Gecko/20100101 Firefox/25.0',
    #'Host': 'sport.mos.ru',
    #'Accept': 'text/html',
    #'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
    'Accept-Encoding': 'gzip',
    'Referer': 'http://sport.mos.ru/',
    #'Connection': 'keep-alive',
    #'Cache-Control': 'max-age=0',
}

BASE_URL = 'http://sport.mos.ru/presscenter/news/'

ul_class = 'newslist'
date_class = 'date'
entry_date_class = 'news_date'


class SportMosRu(grabbers.Parser):

    def run(self, date_from, parsed_link=[]):
        print u'Грабим сайт: {0}'.format(self.domain)

        request = urllib2.Request(BASE_URL, headers=HTTP_HEADERS)
        try:
            page = urllib2.urlopen(request, timeout=10)
        except urllib2.URLError as e:
            print u'Ошибка открытия страницы {0}: {1}'.format(request.get_full_url(), e)
            return

        soup = BeautifulSoup(grabbers.get_page_content(page))
        ul = soup.find('ul', class_=ul_class)

        # в случае если
        if not ul:
            print u'Список новостей не доступен'
            return

        link_to_news = []
        descriptions = []
        for count, li in enumerate(ul.find_all('li')):
            date_span = li.find('span', class_=date_class)
            try:
                date = datetime.datetime.strptime(date_span.string, '%d.%m.%Y').date()
            except ValueError:
                print u'Изменился формат даты в списке новостей'
                continue
            link = 'http://' + self.domain + li.find('a')['href']

            if date >= date_from.date() and link not in parsed_link:
                link_to_news.append(link)
                descriptions.append(li.text)
                print count, li.find('a').text

        for link, description in zip(link_to_news[:5], descriptions[:5]):
            request = urllib2.Request(link, headers=HTTP_HEADERS)
            try:
                page = urllib2.urlopen(request, timeout=10)
            except urllib2.URLError as e:
                print u'Ошибка открытия страницы {0}: {1}'.format(request.get_full_url(), e)
                continue
            soup = BeautifulSoup(grabbers.get_page_content(page))

            h1 = soup.find('h1')

            date_string = soup.find('div', class_=entry_date_class)
            try:
                date = datetime.datetime.strptime(date_span.string, '%d.%m.%Y').date()
            except ValueError:
                print u'Изменился формат даты в новости'
                continue

            article = soup.find('article').text

            image_url = ''
            if soup.find('article').find('img'):
                image_url = 'http://' + self.domain + soup.find('article').find('img')['src']

            self.create_item(title=h1.text,
                             description=description,
                             content=article,
                             image_url='image_url',
                             posted = date,
                             link=link
            )



grabbers.register(SportMosRu('sport.mos.ru'))