__author__ = 'Evgeniy Tatarintsev'
import datetime
from django.core.management.base import NoArgsCommand

from hitcount.models import HitCount


class Command(NoArgsCommand):
    help = "Can be run as a cronjob or directly to update views_count field on objects."

    def handle_noargs(self, **options):
        for hitcount in HitCount.objects.all():
            model_class = hitcount.content_type.model_class()
            object =  model_class.objects.filter(pk=hitcount.object_pk)
            object.update(views_count=hitcount.hits)
