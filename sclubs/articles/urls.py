__author__ = 'Evgeniy Tatarintsev'
from django.conf.urls import patterns, url

from .views import ArticleDetailView, CategoryListView, CategoryDetailView, TaggedArticlesView

urlpatterns = patterns('',
    url(r'^$', CategoryListView.as_view(), name='index'),
    url(r'^category/(?P<slug>[-a-zA-Z0-9_]+)/$', CategoryDetailView.as_view(), name='category'),
    url(r'^tagged/(?P<slug>[-a-zA-Z0-9_]+)/$', TaggedArticlesView.as_view(), name='tagged'),
    url(r'^(?P<slug>[-a-zA-Z0-9_]+)/$', ArticleDetailView.as_view(), name='detail'),
)