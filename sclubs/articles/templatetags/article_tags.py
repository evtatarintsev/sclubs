__author__ = 'Evgeniy Tatarintsev'
from datetime import datetime, timedelta
from django.template import Library
from django.conf import settings
from sclubs.articles.models import Category, Article


register = Library()


@register.assignment_tag
def get_categories():
    return Category.objects.all()


@register.assignment_tag
def get_popular_articles(count=10):
    return Article.objects.most_popular()[:count]


@register.assignment_tag
def read_now_articles(count=10):
    from_time = datetime.today() - timedelta(seconds=settings.READ_NOW_TIMEDELTA_SECONDS)
    return Article.objects.published().filter(last_view__gte=from_time)[:count]

