__author__ = 'Evgeniy Tatarintsev'
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from sclubs.core.models import Displayable, ImagedContent, RichText, Ownable, DisplayableManager
from sclubs.generic.models import Sport, Gallery, Keyword


class ArticleManager(DisplayableManager):
    def most_popular(self):
        return self.order_by('-views_count')


class Category(Displayable, ImagedContent):
    position = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')
        ordering = ['position', ]

    def get_absolute_url(self):
        return reverse('articles:category', kwargs={'slug': self.slug})


class Article(Displayable, ImagedContent, Ownable):
    categories = models.ManyToManyField(Category, verbose_name=_('Categories'), related_name='articles')
    tags = models.ManyToManyField(Keyword, verbose_name=_('Tags'), related_name='articles', null=True, blank=True)
    sports = models.ManyToManyField(Sport, null=True, blank=True,
                                    verbose_name=_('Sports'), related_name='articles')
    gallery = models.ForeignKey(Gallery, verbose_name=_('Gallery'), null=True, blank=True, related_name='+')
    related_articles = models.ManyToManyField('self', verbose_name=_('Related articles'), null=True, blank=True)

    objects = ArticleManager()

    class Meta:
        verbose_name = _('Article')
        verbose_name_plural = _('Articles')

    def get_absolute_url(self):
        return reverse('articles:detail', kwargs={'slug': self.slug})

    def get_description(self):
        return self.meta_description

    def get_tags(self):
        _tags = []
        a = u'<a href="{url}">{tag.title}</a>'

        for tag in self.tags.all():
            url = reverse('articles:tagged', kwargs={'slug': tag.slug})
            _tags.append(a.format(url=url, tag=tag))

        return mark_safe(u', '.join([_tag for _tag in _tags]))

    def categories_as_tags(self):
        _categories = []
        a = u'<a href="{url}">{tag.title}</a>'

        for cat in self.categories.all():
            _categories.append(a.format(url=cat.get_absolute_url(), tag=cat))
        print self.categories.all()
        return mark_safe(u', '.join([cat for cat in _categories]))

    def get_related(self):
        related = Article.objects.filter(tags__in=self.tags.all())
        related |= self.related_articles.all()
        return related.exclude(pk=self.pk).distinct()


class Aphorism(Displayable, RichText):
    author = models.CharField(_('Author'), max_length=100, null=True, blank=True)

    class Meta:
        verbose_name = _('Aphorism')
        verbose_name_plural = _('Aphorisms')
