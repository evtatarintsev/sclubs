__author__ = 'Evgeniy Tatarintsev'
from django.utils.timezone import now
from django.views.generic import ListView, DetailView
from django.http import Http404

from models import Article, Aphorism, Category
from sclubs.generic.models import Keyword


class ArticleDetailView(DetailView):
    def get_queryset(self):
        return Article.objects.published()

    def get_object(self, queryset=None):
        article = self.get_queryset().filter(slug=self.kwargs['slug'])
        if not article:
            raise Http404
        article.update(last_view=now())
        return article[0]


class CategoryDetailView(DetailView):
    def get_queryset(self):
        return Category.objects.published()


class CategoryListView(ListView):
    def get_queryset(self):
        return Category.objects.published()


class TaggedArticlesView(DetailView):
    model = Keyword
    template_name = 'articles/tagged_list.html'
    context_object_name = 'tag'