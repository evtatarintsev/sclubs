__author__ = 'Evgeniy Tatarintsev'
from django.contrib import admin
from django import forms
from django.utils.translation import ugettext as _

from .models import Article, Aphorism, Category


class ArticleAdminForm(forms.ModelForm):
    class Meta:
        model = Article

    def __init__(self, *args, **kwargs):
        super(ArticleAdminForm, self).__init__(*args, **kwargs)
        self.fields['meta_description'].required = True


class ArticleAdmin(admin.ModelAdmin):
    form = ArticleAdminForm
    list_display = ('title', 'categories_string', 'admin_link',
                    'created', 'updated', 'is_published', 'user', 'views_count')

    list_filter = ('is_published', 'categories', 'sports', )
    search_fields = ('title', 'meta_description', 'meta_title', 'meta_keywords', 'content')
    filter_horizontal = ('sports', 'categories', 'tags', 'related_articles')
    raw_id_fields = ('gallery', )
    readonly_fields = ('last_view', )

    fieldsets = (
        (None, {'fields':('title', 'meta_description', 'image', 'content',
                          ('is_published', 'in_navigation'), 'tags', 'sports', 'categories', 'related_articles', 'gallery')}),
        (_('Dates'), {'fields':('publish_date', 'expiry_date', 'last_view'),
                             'classes': ['wide', 'extrapretty'],}),
        (_('SEO'), {'fields':('meta_title', 'meta_keywords', 'slug'),
                    'classes': ['collapse'],}),
    )

    def categories_string(self, obj):
        return ", ".join([cat.title for cat in obj.categories.all()])
    categories_string.allow_tags = True
    categories_string.short_description = _('Categories')

    def admin_link(self, obj):
        return "<a href='%s?preview' target='_blank'>%s</a>" % (obj.get_absolute_url(), _("View on site"))
    admin_link.allow_tags = True
    admin_link.short_description = ""

    def save_model(self, request, obj, form, change):
        if not obj.user_id:
            obj.user = request.user
        super(ArticleAdmin, self).save_model(request, obj, form, change)


class CategoryAdminForm(forms.ModelForm):
    class Meta:
        model = Category

    def __init__(self, *args, **kwargs):
        super(CategoryAdminForm, self).__init__(*args, **kwargs)
        self.fields['meta_description'].required = True
        self.fields['content'].required = False


class CategoryAdmin(admin.ModelAdmin):
    form = CategoryAdminForm
    list_filter = ('is_published', )
    list_display = ('title', 'position', 'is_published')
    list_editable = ('position', 'is_published')

    fieldsets = (
        (None, {'fields': ('title', 'meta_description', 'image', 'content',
                          'is_published')}),
        (_('Dates'), {'fields': ('publish_date', 'expiry_date',),
                             'classes': ['wide', 'extrapretty'],}),
        (_('SEO'), {'fields': ('meta_title', 'meta_keywords', 'slug', ),
                    'classes': ['collapse'],}),
    )

    class Media:
        js = (
            'js/admin_list_reorder.js',
        )


class AphorismAdmin(admin.ModelAdmin):
    list_filter = ('is_published', )

    fieldsets = (
        (None, {'fields': ('title', 'meta_description', 'content',
                          'author', 'is_published',)}),
        (_('Dates'), {'fields': ('publish_date', 'expiry_date',),
                             'classes': ['wide', 'extrapretty'],}),
        (_('SEO'), {'fields': ('meta_title', 'meta_keywords', 'slug', ),
                    'classes': ['collapse'],}),
    )

admin.site.register(Category, CategoryAdmin)
admin.site.register(Article, ArticleAdmin)
admin.site.register(Aphorism, AphorismAdmin)