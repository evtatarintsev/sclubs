__author__ = 'john'
import os.path
from django.contrib.comments.models import Comment
from django.contrib.contenttypes.generic import GenericForeignKey
from django.db import models
from django.template.defaultfilters import truncatewords_html
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib.auth import get_user_model

from mptt.models import MPTTModel

from sclubs.generic.fields import RatingField
from sclubs.generic.managers import CommentManager, KeywordManager
from sclubs.core.models import Slugged
from sclubs.utils.sites import current_site_id
import sclubs.settings.conf as settings

SPORT_CHOOSER_COLUMNS = (
    (1, '1'),
    (2, '2'),
    (3, '3'),
    (4, '4'),
)


class ThreadedComment(Comment):
    """
    Extend the ``Comment`` model from ``django.contrib.comments`` to
    add comment threading. ``Comment`` provides its own site foreign key,
    so we can't inherit from ``SiteRelated`` in ``mezzanine.core``, and
    therefore need to set the site on ``save``. ``CommentManager``
    inherits from Mezzanine's ``CurrentSiteManager``, so everything else
    site related is already provided.
    """

    by_author = models.BooleanField(_("By the blog author"), default=False)
    replied_to = models.ForeignKey("self", null=True, editable=False,
                                   related_name="comments")
    rating = RatingField(verbose_name=_("Rating"))

    objects = CommentManager()

    class Meta:
        verbose_name = _("Comment")
        verbose_name_plural = _("Comments")

    def get_absolute_url(self):
        """
        Use the URL for the comment's content object, with a URL hash
        appended that references the individual comment.
        """
        url = self.content_object.get_absolute_url()
        return "%s#comment-%s" % (url, self.id)

    def save(self, *args, **kwargs):
        """
        Set the current site ID, and ``is_public`` based on the setting
        ``COMMENTS_DEFAULT_APPROVED``.
        """
        if not self.id:
            self.is_public = settings.COMMENTS_DEFAULT_APPROVED
            self.site_id = current_site_id()
        super(ThreadedComment, self).save(*args, **kwargs)

    ################################
    # Admin listing column methods #
    ################################

    def intro(self):
        return truncatewords_html(self.comment, 20)
    intro.short_description = _("Comment")

    def avatar_link(self):
        vars = (self.user_email, gravatar_url(self.email), self.user_name)
        return ("<a href='mailto:%s'><img style='vertical-align:middle; "
                "margin-right:3px;' src='%s' />%s</a>" % vars)
    avatar_link.allow_tags = True
    avatar_link.short_description = _("User")

    def admin_link(self):
        return "<a href='%s'>%s</a>" % (self.get_absolute_url(),
                                        ugettext("View on site"))
    admin_link.allow_tags = True
    admin_link.short_description = ""

    # Exists for backward compatibility when the gravatar_url template
    # tag which took the email address hash instead of the email address.
    @property
    def email_hash(self):
        return self.email


class Keyword(Slugged):
    """
    Keywords/tags which are managed via a custom JavaScript based
    widget in the admin.
    """

    objects = KeywordManager()

    class Meta:
        verbose_name = _("Keyword")
        verbose_name_plural = _("Keywords")
        ordering = ['title', ]


class AssignedKeyword(models.Model):
    """
    A ``Keyword`` assigned to a model instance.
    """

    keyword = models.ForeignKey("Keyword", verbose_name=_("Keyword"),
                                related_name="assignments")
    content_type = models.ForeignKey("contenttypes.ContentType")
    object_pk = models.IntegerField()
    content_object = GenericForeignKey("content_type", "object_pk")

    #class Meta:
    #    order_with_respect_to = "content_object"

    def __unicode__(self):
        return unicode(self.keyword)


class Rating(models.Model):
    """
    A rating that can be given to a piece of content.
    """

    value = models.IntegerField(_("Value"))
    rating_date = models.DateTimeField(_("Rating date"),
        auto_now_add=True, null=True)
    content_type = models.ForeignKey("contenttypes.ContentType")
    object_pk = models.IntegerField()
    content_object = GenericForeignKey("content_type", "object_pk")
    user = models.ForeignKey(get_user_model(), verbose_name=_("Rater"),
        null=True, related_name="%(class)ss")

    class Meta:
        verbose_name = _("Rating")
        verbose_name_plural = _("Ratings")

    def save(self, *args, **kwargs):
        """
        Validate that the rating falls between the min and max values.
        """
        valid = map(str, settings.RATINGS_RANGE)
        if str(self.value) not in valid:
            raise ValueError("Invalid rating. %s is not in %s" % (self.value,
                ", ".join(valid)))
        super(Rating, self).save(*args, **kwargs)


class SportCategory(Slugged):
    column = models.PositiveSmallIntegerField(_('Column for display category'),
                                              choices=SPORT_CHOOSER_COLUMNS,
                                              null=True, blank=True)

    class Meta:
        verbose_name = _("Sport category")
        verbose_name_plural = _("Sport categories")


class Sport(Slugged):
    logo = models.ImageField(_('Logo'), upload_to='sports',
                             null=True, blank=True)
    categories = models.ManyToManyField(SportCategory, verbose_name=_('Categories'), related_name='sports')


    class Meta:
        verbose_name = _("Sport")
        verbose_name_plural = _("Sports")


class Location(Slugged):
    parent = models.ForeignKey('self', related_name='children',
                               verbose_name=_('Parent'), null=True, blank=True)

    class Meta:
        verbose_name = _("Location")
        verbose_name_plural = _("Locations")


def dir_for_gallery(instance, filename):
        return os.path.join('galleries', instance.gallery.slug, filename)


class Gallery(Slugged):
    class Meta:
        verbose_name = _('Gallery')
        verbose_name_plural = _('Galleries')


class Image(models.Model):
    gallery = models.ForeignKey(Gallery, verbose_name=_('Gallery'), related_name='images')
    image = models.ImageField(_('Image'), upload_to=dir_for_gallery, max_length=255)
    alt = models.TextField(_('Text (alt)'), null=True, blank=True)



