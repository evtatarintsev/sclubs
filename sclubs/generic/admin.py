__author__ = 'Evgeniy Tatarintsev'
from django.contrib import admin
from django.contrib.comments.admin import CommentsAdmin
from django.utils.translation import ugettext_lazy as _

from sclubs.generic.models import (ThreadedComment, Sport, SportCategory,
                                   Location, Keyword, Gallery, Image)


class ThreadedCommentAdmin(CommentsAdmin):
    """
    Admin class for comments.
    """

    list_display = ("avatar_link", "intro", "submit_date", "is_public",
                    "is_removed", "admin_link")
    list_display_links = ("intro", "submit_date")
    list_filter = [f for f in CommentsAdmin.list_filter if f != "site"]
    fieldsets = (
        (_("User"), {"fields": ("user_name", "user_email", "user_url")}),
        (None, {"fields": ("comment", ("is_public", "is_removed"))}),
    )

    def get_actions(self, request):
        actions = super(CommentsAdmin, self).get_actions(request)
        actions.pop("delete_selected")
        actions.pop("flag_comments")
        return actions


class SportAdmin(admin.ModelAdmin):
    list_filter = ('categories', )
    filter_horizontal = ('categories', )


class LocationAdmin(admin.ModelAdmin):
    raw_id_fields = ('parent', )


class KeywordAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug')


class ImageAdminInline(admin.TabularInline):
    model = Image


class GalleryAdmin(admin.ModelAdmin):
    inlines = (ImageAdminInline, )


admin.site.register(ThreadedComment, ThreadedCommentAdmin)
admin.site.register(Sport, SportAdmin)
admin.site.register(SportCategory)
admin.site.register(Location, LocationAdmin)
admin.site.register(Keyword, KeywordAdmin)
admin.site.register(Gallery, GalleryAdmin)