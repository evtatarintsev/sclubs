__author__ = 'Evgeniy Tatarintsev'
from django import template

from sclubs.generic.models import SportCategory


register = template.Library()


@register.inclusion_tag('sport_chooser.html')
def sport_chooser(base_url):
    columns = []
    for i in range(1, 5):
        columns.append(SportCategory.objects.filter(column=i))

    return {'columns':columns}