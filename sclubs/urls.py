__author__ = 'Evgeniy Tatarintsev'
from django.conf.urls import patterns, url, include
from django.contrib import admin
from django.conf import settings

from sclubs.generic.views import HomePageView

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^hit/update/$', 'hitcount.views.update_hit_count_ajax',
                                            name='hitcount_update_ajax'),

    url(r'^$', HomePageView.as_view(), name='home-page'),
    url(r'^news/', include('sclubs.news.urls', namespace='news')),
    url(r'^articles/', include('sclubs.articles.urls', namespace='articles')),
    url(r'^clubs/', include('sclubs.clubs.urls', namespace='clubs')),
    url(r'^accounts/', include('sclubs.accounts.urls', namespace='accounts')),

)


if settings.DEBUG:
    urlpatterns += patterns("",
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
            'show_indexes': True}),
    )