__author__ = 'Evgeniy Tatarintsev'
from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from .views import (ClubDetailView,
                    ClubsIndexView,
                    ClubsBySportView,
                    ClubCoachesView,
                    CreateClubView)

urlpatterns = patterns('',
    url(r'^$', ClubsIndexView.as_view(), name='index'),
    url(r'^create/$', login_required(CreateClubView.as_view()), name='create'),
    #url(r'^category/(?P<slug>[-a-zA-Z0-9_]+)/$', CategoryDetailView.as_view(), name='category'),
    url(r'^sport/(?P<slug>[-a-zA-Z0-9_]+)/$', ClubsBySportView.as_view(), name='sport'),
    url(r'^(?P<slug>[-a-zA-Z0-9_]+)/$', ClubDetailView.as_view(), name='detail'),
    url(r'^(?P<slug>[-a-zA-Z0-9_]+)/coaches/$', ClubCoachesView.as_view(), name='coaches'),
)