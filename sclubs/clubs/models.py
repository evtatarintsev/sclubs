#-*- coding: utf-8 -*-
__author__ = 'Evgeniy Tatarintsev'
import os.path
import uuid
from postmarkup import render_bbcode
from django.utils.translation import ugettext, ugettext_lazy as _
from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model

from sclubs.core.models import Displayable
from sclubs.generic.models import Sport
from sclubs.core.fields import RichTextField


def upload_to_club(instance, filename):
    ext = filename.split('.')[-1]
    filename = '%s.%s'%(uuid.uuid4(), ext)
    return os.path.join('clubs', instance.slug, filename)


def upload_to_album(instance, filename):
    ext = filename.split('.')[-1]
    filename = '%s.%s'%(uuid.uuid4(), ext)
    return os.path.join('clubs', instance.album.club.slug, 'albums', instance.album.slug, filename)


class Coach(models.Model):
    name = models.CharField(_('Name'), max_length=80)
    user = models.ForeignKey(get_user_model(), verbose_name=_('User'), related_name='+', null=True, blank=True)
    club = models.ForeignKey('Club', verbose_name=_('Club'), related_name='coaches')

    class Meta:
        verbose_name = _('Coach')
        verbose_name_plural = _('Coaches')


class Address(models.Model):
    street = models.CharField(_('Street'), max_length=255)
    phone = models.CharField(_('Phone'), max_length=255)
    club = models.ForeignKey('Club', verbose_name=_('Club'))


class Club(Displayable):
    logo = models.ImageField(_('Logo'), upload_to=upload_to_club)
    sports = models.ManyToManyField(Sport, verbose_name=_('Sports'), related_name='clubs')
    content = RichTextField(_('Content'), config_name='bbcode')
    html_content = RichTextField(_('HTML Content'), blank=True)
    admins = models.ManyToManyField(get_user_model(), verbose_name=_('Admins'), related_name='clubs')

    class Meta:
        verbose_name = _('Club')
        verbose_name_plural = _('Clubs')

    def get_absolute_url(self):
        return reverse('clubs:detail', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        self.html_content = render_bbcode(self.content)
        return super(Club, self).save(*args, **kwargs)


class Album(Displayable):
    club = models.ForeignKey('Club', verbose_name=_('Club'), related_name='albums')

    class Meta:
        verbose_name = _('Album')
        verbose_name_plural = _('Albums')

    def get_absolute_url(self):
        return reverse('clubs:detail', kwargs={'slug': self.slug})


class Image(Displayable):
    image = models.ImageField(_('Image'), upload_to=upload_to_album)
    album = models.ForeignKey(Album, verbose_name=_('Album'), related_name='images')

    class Meta:
        verbose_name = _('Image')
        verbose_name_plural = _('Image')

    def get_absolute_url(self):
        return reverse('clubs:detail', kwargs={'slug': self.slug})
