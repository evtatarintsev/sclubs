#-*- coding: utf-8 -*-
__author__ = 'Evgeniy Tatarintsev'
from django import forms

from .models import Club


class ClubForm(forms.ModelForm):
    class Meta:
        model = Club
        fields = ('title', 'logo', 'content', 'sports')