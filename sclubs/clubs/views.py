#-*- coding: utf-8 -*-
__author__ = 'Evgeniy Tatarintsev'
from django.views.generic import DetailView, ListView, CreateView, UpdateView
from django.http import Http404
from django.db.models import Q

from sclubs.generic.models import Sport, SportCategory
from .models import Club
from .forms import ClubForm

class ClubsIndexView(ListView):
    def get_queryset(self):
        return Club.objects.published()


class ClubDetailView(DetailView):
    def get_queryset(self):
        return Club.objects.published()


class ClubsBySportView(ListView):
    template_name = 'clubs/clubs_by_sport.html'

    def dispatch(self, request, *args, **kwargs):
        self.sport = None
        self.sport_category = None

        try:
            self.sport = Sport.objects.get(slug=self.kwargs['slug'])
        except Sport.DoesNotExist:
            try:
                self.sport_category = SportCategory.objects.get(slug=self.kwargs['slug'])
            except SportCategory.DoesNotExist:
                raise Http404
        return super(ClubsBySportView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        if self.sport:
            return Club.objects.published().filter(sports=self.sport)

        return Club.objects.published().filter(sports__categories=self.sport_category).distinct()


class ClubCoachesView(ListView):
    def dispatch(self, request, *args, **kwargs):
        self.club = Club.objects.published().get(slug=kwargs['slug'])
        return super(ClubCoachesView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return self.club.coaches.all()

    def get_context_data(self, **kwargs):
        ctx = super(ClubCoachesView, self).get_context_data(**kwargs)
        ctx['club'] = self.club
        return ctx


class CreateClubView(CreateView):
    form_class = ClubForm
    model = Club

