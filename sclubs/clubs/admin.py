#-*- coding: utf-8 -*-
__author__ = 'Evgeniy Tatarintsev'
from django.contrib import admin
from django import forms
from django.utils.translation import ugettext as _

from .models import Club, Address, Coach


class AddressAdminInline(admin.TabularInline):
    model = Address
    extra = 1

class CoachAdminInline(admin.TabularInline):
    model = Coach
    extra = 1

class ClubAdminForm(forms.ModelForm):
    class Meta:
        model = Club

    def __init__(self, *args, **kwargs):
        super(ClubAdminForm, self).__init__(*args, **kwargs)


class ClubAdmin(admin.ModelAdmin):
    form = ClubAdminForm
    inlines = (AddressAdminInline, CoachAdminInline)
    filter_horizontal = ('sports', 'admins', )
    list_display = ('title', 'created')
    fieldsets = (
        (None, {'fields':('title',  'logo', 'content', 'html_content',
                          'is_published', 'sports', 'admins', )}),
        (_('Dates'), {'fields':('publish_date', 'expiry_date',),
                             'classes': ['wide', 'extrapretty'],}),
        (_('SEO'), {'fields':('meta_title', 'meta_description', 'meta_keywords', 'slug', ),
                    'classes': ['collapse'],}),
    )



admin.site.register(Club, ClubAdmin)