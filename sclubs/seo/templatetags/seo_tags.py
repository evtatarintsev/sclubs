__author__ = 'Evgeniy Tatarintsev'
from django import template
from sclubs.seo.models import MetaTags
from sclubs.core import meta_tags_template
register = template.Library()


class MetaTagsNode(template.Node):
    def __init__(self, full_path):
        self.full_path = template.Variable(full_path)

    def render(self, context):
        try:
            full_path = self.full_path.resolve(context)
            if '?' in full_path:
                path = full_path.split('?')[0]
            else:
                path = full_path
        except template.VariableDoesNotExist:
            return ''

        try:
            tags = MetaTags.objects.get(page_url=full_path)
        except MetaTags.DoesNotExist:
            try:
                tags = MetaTags.objects.get(page_url=path)
            except MetaTags.DoesNotExist:
                tags = MetaTags.objects.create(page_url=path, name=path, title=path, description=path)

        render_tags = meta_tags_template.format(tags.title, tags.description, tags.keywords)
        return render_tags


def get_meta_tags(parser, token):
    try:
        tokens = token.split_contents()
        tag_name, for_string, full_path = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError, '%r tag format is {% meta_tags for "url" %}' % token.contents.split()[0]

    return MetaTagsNode(full_path)


register.tag('meta_tags', get_meta_tags)