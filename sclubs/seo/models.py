__author__ = 'Evgeniy Tatarintsev'
from django.db import models
from django.utils.translation import ugettext as _


class MetaTags(models.Model):
    name = models.CharField(_('Verbose name'), max_length=250)
    page_url = models.CharField(_('Page URL'), max_length=250, db_index=True)

    title = models.CharField(_('Page title'), max_length=100)
    description = models.TextField(_('Page description'))
    keywords = models.CharField(_('Page keywords'), max_length=100, blank=True, null=True)

    class Meta:
        verbose_name = _('Meta tags')
        verbose_name_plural = _('Meta tags')

    def __unicode__(self):
        return self.name

