__author__ = 'Evgeniy Tatarintsev'
from django.contrib import admin

from .models import MetaTags


class MetaTagsAdmin(admin.ModelAdmin):
    list_display = ('name', 'page_url', 'title', 'description', 'keywords')
    search_fields = ('name', 'page_url', 'title', 'description', 'keywords')

admin.site.register(MetaTags, MetaTagsAdmin)