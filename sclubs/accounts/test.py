#-*- coding: utf-8 -*-
__author__ = 'Evgeniy Tatarintsev'
from django.test import TestCase
from django.test.client import Client

from .models import User

class AuthViewTest(TestCase):
    def setUp(self):
        result = self.client.login(email='testuser@mail.ru', password='secret')
        self.assertTrue(result)

