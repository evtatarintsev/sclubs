__author__ = 'Evgeniy Tatarintsev'
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.utils.http import urlquote
from django.utils import timezone

from sclubs.settings.conf import DEFAULT_USER_ID_LENGTH, USER_ID_PREFIX
from .managers import UserManager


def gen_user_id(pk):
    """
    Generate verbose user id
    """
    blank_space = DEFAULT_USER_ID_LENGTH - len(USER_ID_PREFIX) - len(str(pk))
    return USER_ID_PREFIX + \
           ''.join(['0' for i in range(blank_space)]) + \
           str(pk)


class User(AbstractBaseUser, PermissionsMixin):
    """
    User object for sclubs project
    use email as username
    """
    email = models.EmailField(_('email address'), unique=True, db_index=True,
                              null=True, blank=True)
    slug = models.SlugField(_('Verbose user ID'), max_length=50, unique=True)
    is_active = models.BooleanField(_('active'), default=True,
        help_text=_('Designates whether this user should be treated as '
                    'active. Unselect this instead of deleting accounts.'))
    is_staff = models.BooleanField(_('staff status'), default=False,
        help_text=_('Designates whether the user can log into this admin '
                    'site.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)



    USERNAME_FIELD = 'email'
    objects = UserManager()

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)

        if not self.slug:
            self.slug = gen_user_id(self.pk)
            self.save(update_fields=['slug', ])

    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        Email is sent asynchronously
        """
        raise NotImplementedError

    def get_absolute_url(self):
        return "/users/%s/" % urlquote(self.username)

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        return self.email

    def get_short_name(self):
        "Returns the short name for the user."
        return self.email