#-*- coding: utf-8 -*-
__author__ = 'Evgeniy Tatarintsev'
from django.contrib import admin
from django import forms
from django.contrib import auth
from django.utils.translation import ugettext, ugettext_lazy as _
from models import User


class UserChangeForm(auth.admin.UserChangeForm):
    class Meta:
        model = User


class UserCreationForm(auth.admin.UserCreationForm):
    class Meta:
        model = User

    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            User._default_manager.get(email=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(
            self.error_messages['duplicate_username'],
            code='duplicate_username',
        )


class UserAdmin(auth.admin.UserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'password')}),

        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', )}),
    )

    form = UserChangeForm
    add_form = UserCreationForm
    filter_horizontal = ('groups', 'user_permissions', )
    search_fields = ('email', )

    list_display = ('email', 'is_staff')
    ordering = ['email', ]


admin.site.register(User, UserAdmin)
