__author__ = 'Evgeniy Tatarintsev'
# -*-coding: utf-8-*-
from django.conf.urls import patterns, url, include
from django.views.generic import DetailView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import logout
from .views import UserProfileView
from .models import User


user_related_urls = patterns('',
    url(r'^$', UserProfileView.as_view(template_name='accounts/profile.html'), name='profile'),
    url(r'^clubs/$', UserProfileView.as_view(template_name='accounts/club_list.html'), name='clubs'),
)

urlpatterns = patterns('',
    #url(r'^login/$', LoginView.as_view(), name='login'),
    #url(r'^', include('social_auth.urls')),
    url(r'^logout/$', login_required(logout), name='logout'),

    url(r'^(?P<pk>\d+)/', include(user_related_urls)),


    ##url(r'^(?P<pk>\d+)/events/$', login_required(UserDetailView.as_view(model=User, template_name='accounts/event_list.html')), name='events'),
    #url(r'^(?P<pk>\d+)/news/$', login_required(UserDetailView.as_view(model=User, template_name='accounts/news_list.html')), name='news'),
    #url(r'^(?P<pk>\d+)/settings/$', login_required(SettingsView.as_view()), name='settings'),

    #url(r'^(?P<pk>\d+)/mail/$', login_required(UserDetailView.as_view(model=User, template_name='accounts/mail.html')), name='mail'),

)
