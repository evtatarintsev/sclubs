#-*- coding: utf-8 -*-
__author__ = 'Evgeniy Tatarintsev'
from django.views.generic import DetailView

from .models import User


class UserProfileView(DetailView):
    context_object_name = "profile_owner"

    def get_queryset(self):
        return User.objects.active()

    def get_context_data(self, **kwargs):
        ctx = super(UserProfileView, self).get_context_data(**kwargs)
        ctx['can_edit'] = self.request.user == self.object
        return ctx