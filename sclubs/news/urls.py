__author__ = 'Evgeniy Tatarintsev'
from django.conf.urls import patterns, url

from .views import EntryListView, EntryDetail

urlpatterns = patterns('',
    url(r'^$', EntryListView.as_view(), name='index'),
    url(r'^(?P<slug>[-a-zA-Z0-9_]+)/$', EntryDetail.as_view(), name='detail'),
)