__author__ = 'Evgeniy Tatarintsev'
from django.contrib import admin
from django import forms
from django.utils.translation import ugettext as _

from ckeditor.widgets import CKEditorWidget
from .models import Entry


class EntryAdminForm(forms.ModelForm):
    class Meta:
        model = Entry
        widgets = {
            'content': CKEditorWidget(),
        }

    def __init__(self, *args, **kwargs):
        super(EntryAdminForm, self).__init__(*args, **kwargs)
        self.fields['meta_description'].required = True


class EntryAdmin(admin.ModelAdmin):
    form = EntryAdminForm
    list_display = ('title', 'admin_link', 'created', 'is_published', 'user',)
    list_filter = ('is_published', 'sports')
    search_fields = ('title', 'meta_description', 'meta_title', 'meta_keywords', 'content')
    filter_horizontal = ('sports', )
    fieldsets = (
        (None, {'fields':('title', 'meta_description', 'image', 'content',
                          'is_published', 'tags', 'sports', 'gallery')}),
        (_('For hot news'), {'fields':('publish_date', 'expiry_date',),
                             'classes': ['wide', 'extrapretty'],}),
        (_('SEO'), {'fields':('meta_title', 'meta_keywords', 'slug', ),
                    'classes': ['collapse'],}),
    )

    def admin_link(self, obj):
        return "<a href='%s?preview' target='_blank'>%s</a>" % (obj.get_absolute_url(), _("View on site"))
    admin_link.allow_tags = True
    admin_link.short_description = ""

    def save_model(self, request, obj, form, change):
        if not obj.user_id:
            obj.user = request.user
        super(EntryAdmin, self).save_model(request, obj, form, change)



admin.site.register(Entry, EntryAdmin)