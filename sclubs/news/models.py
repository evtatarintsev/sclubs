__author__ = 'john'
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now
from django.core.urlresolvers import reverse
from django.core.cache import cache
from django.utils.safestring import mark_safe

from sclubs.core.models import ImagedContent, Ownable, Displayable
from sclubs.generic.models import Sport, Gallery
from sclubs.generic.fields import KeywordsField


class NewsManager(models.Manager):
    def active(self, **kwargs):
        return self.filter(is_published=True, publish_date__lte=now(), **kwargs)

    def hot_news(self, **kwargs):
        return self.active(expiry_date_gte=now(), **kwargs)


class Entry(Displayable, ImagedContent, Ownable):

    tags = KeywordsField()
    sports = models.ManyToManyField(Sport, null=True, blank=True,
                                    verbose_name=_('Sports'), related_name='news')
    source = models.URLField(_('Source'), null=True, blank=True)
    gallery = models.ForeignKey(Gallery, verbose_name=_('Gallery'), null=True, blank=True, related_name='+')

    objects = NewsManager()

    class Meta:
        verbose_name = _('Entry')
        verbose_name_plural = _('News')
        ordering = ['-publish_date', ]

    def get_absolute_url(self):
        return reverse('news:detail', kwargs={'slug': self.slug})

    def get_description(self):
        return self.meta_description

    def get_tags(self):
        tags = cache.get('tags_for_entry_id_{pk}'.format(pk=self.pk)) or ''
        if tags:
            return mark_safe(tags)

        news_url = reverse('news:index')
        tags = ', '.join([u'<a href="{0}?sport={1.slug}">{1.title}</a>'.format(news_url, sport) for sport in self.sports.all()])

        return mark_safe(tags)


