__author__ = 'john'
from django.views.generic import ListView, DetailView
from django.conf import settings

from .models import Entry


class EntryListView(ListView):
    paginate_by = getattr(settings, 'NEWS_ON_PAGE', 10)

    def get_queryset(self):
        filters = {}

        if 'sport' in self.request.GET:
            filters['sports__slug'] = self.request.GET['sport']

        return Entry.objects.active(**filters)


class EntryDetail(DetailView):
    def get_queryset(self):
        if 'preview' in self.request.GET and self.request.user.is_staff:
            return Entry.objects.all()

        return Entry.objects.active()