__author__ = 'Evgeniy Tatarintsev'
import re
import unicodedata

from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import (resolve, reverse, NoReverseMatch,
                                      get_script_prefix)
from django.shortcuts import redirect
from django.utils.encoding import smart_unicode
from django.utils import translation

from sclubs import settings
from sclubs.utils.importing import import_dotted_path


def admin_url(model, url, object_id=None):
    """
    Returns the URL for the given model and admin url name.
    """
    opts = model._meta
    url = "admin:%s_%s_%s" % (opts.app_label, opts.object_name.lower(), url)
    args = ()
    if object_id is not None:
        args = (object_id,)
    return reverse(url, args=args)


def home_slug():
    """
    Returns the slug arg defined for the ``home`` urlpattern, which
    is the definitive source of the ``url`` field defined for an
    editable homepage object.
    """
    prefix = get_script_prefix()
    slug = reverse("home")
    if slug.startswith(prefix):
        slug = '/' + slug[len(prefix):]
    try:
        return resolve(slug).kwargs["slug"]
    except KeyError:
        return slug




def unique_slug(queryset, slug_field, slug):
    """
    Ensures a slug is unique for the given queryset, appending
    an integer to its end until the slug is unique.
    """
    i = 0
    while True:
        if i > 0:
            if i > 1:
                slug = slug.rsplit("-", 1)[0]
            slug = "%s-%s" % (slug, i)
        try:
            queryset.get(**{slug_field: slug})
        except ObjectDoesNotExist:
            break
        i += 1
    return slug


def login_redirect(request):
    """
    Returns the redirect response for login/signup. Favors:
    - next param
    - LOGIN_REDIRECT_URL setting
    - homepage
    """
    ignorable_nexts = ("",)
    if "sclubs.accounts" in settings.INSTALLED_APPS:
        from sclubs.accounts import urls
        ignorable_nexts += (urls.SIGNUP_URL, urls.LOGIN_URL, urls.LOGOUT_URL)
    next = request.REQUEST.get("next", "")
    if next in ignorable_nexts:
        try:
            next = reverse(settings.LOGIN_REDIRECT_URL)
        except NoReverseMatch:
            next = "/"
    return redirect(next)


def path_to_slug(path):
    """
    Removes everything from the given URL path, including
    language code and ``PAGES_SLUG`` if any is set, returning
    a slug that would match a ``Page`` instance's slug.
    """
    from mezzanine.urls import PAGES_SLUG
    lang_code = translation.get_language_from_path(path)
    for prefix in (lang_code, settings.SITE_PREFIX, PAGES_SLUG):
        if prefix:
            path = path.replace(prefix, "", 1)
    return path.strip("/") or "/"
