$(document).ready(function(){
	
 $("#id_city").fcbkcomplete({
    json_url:'{% url "get-city-list" %}',
    first_selected:false,
    filter_hide : true,
    filter_case:false,
    bricket: false,
    maxitems: 100,
    complete_text:"Выберите города",
    });
  
});