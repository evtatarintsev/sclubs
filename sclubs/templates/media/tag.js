$(document).ready(function(){
$('#id_tags').autocomplete({
    serviceUrl: 'service/autocomplete.ashx', // Страница для обработки запросов автозаполнения
    minChars: 2, // Минимальная длина запроса для срабатывания автозаполнения
    delimiter: /(,|;)\s*/, // Разделитель для нескольких запросов, символ или регулярное выражение
    maxHeight: 400, // Максимальная высота списка подсказок, в пикселях
    width: 256, // Ширина списка
    zIndex: 9999, // z-index списка
    deferRequestBy: 0, // Задержка запроса (мсек), на случай, если мы не хотим слать миллион запросов, пока пользователь печатает. Я обычно ставлю 300.
    params: { country: 'Yes'}, // Дополнительные параметры
    onSelect: function(data, value){ }, // Callback функция, срабатывающая на выбор одного из предложенных вариантов,
    lookup: [{% for tag in tag_list %}"{{tag.name}},",{% endfor %}""] // Список вариантов для локального автозаполнения
});
});
