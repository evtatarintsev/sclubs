$(document).ready(function(){
 $("#id_city").fcbkcomplete({
    json_url:'{% url "get-city-list" %}',
    first_selected:false,
    filter_hide : true,
    filter_case:false,
    bricket: false,
    maxitems: 1,
    complete_text:"Введите название города",
    });
  
});