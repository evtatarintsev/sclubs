$(document).ready(function(){

 $("#id_sport").fcbkcomplete({
    json_url:'{% url "get-sport-list" %}',
    first_selected:false,
    filter_hide : true,
    filter_case:false,
    bricket: false,
    maxitems: 1,
    complete_text:"Введите вид спорта",
    });

});