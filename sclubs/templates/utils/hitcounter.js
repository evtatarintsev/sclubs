<script type="text/javascript">
    $(document).ready(function() {
        $.post( '{{ update_url }}',
	            { hitcount_pk : '{{ hitcount.pk }}',
                  'csrfmiddlewaretoken':'{{ csrfmiddlewaretoken }}'  },
                function(data, status) {
                    if (data.status == 'error') {
                        // do something for error?
                    }
                },
	            'json');
        });
</script>