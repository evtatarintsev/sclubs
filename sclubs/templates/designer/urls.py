from django.conf.urls import patterns, include, url

from django.conf import settings
from django.contrib import admin
admin.autodiscover()

from {{domain}}.index import HomeView

urlpatterns = patterns('',
    (r'^grappelli/', include('grappelli.urls')),
    #url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^articles/', include('scms.articles.urls', namespace='articles')),
    url(r'^news/', include('scms.news.urls', namespace='news')),
    url(r'^gallery/', include('scms.gallery.urls', namespace='gallery')),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('scms.static_page.urls', namespace='static-pages')),
)
