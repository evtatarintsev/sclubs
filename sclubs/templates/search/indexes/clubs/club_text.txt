{% load comments %}

{{ object.title }}

{% for sport in object.sports.all %}
	{{ sport }},
{% endfor %}

{{ object.city }}
{{ object.address }}
{{ object.description }}

{% render_comment_list for object %}
