__author__ = 'Evgeniy Tatarintsev'
import os.path
from django.utils.translation import ugettext as _

APPEND_SLASH = True

ADMINS = (
    ('John', 'evtatarintsev@ya.ru'),
)

MANAGERS = ADMINS

# ---------------------------------------
DEFAULT_FROM_EMAIL = 'info@sclubs.ru'
EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_PORT = '25'
EMAIL_HOST_USER = 'info@sclubs.ru'

SERVER_EMAIL = 'errors@sclubs.ru'
#----------------------------------------

LANGUAGE_CODE = 'ru-Ru'
LANGUAGES = (
    ('ru', _('Russian')),
    ('en', _('English')),
)


SITE_ID = 1

USE_I18N = True
USE_L10N = True

USE_TZ = True
TIME_ZONE = 'Europe/Moscow'

MEDIA_URL = '/media/'
STATIC_URL = '/static/'


AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)


STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)


TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)


MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.redirects.middleware.RedirectFallbackMiddleware'
)


TEMPLATE_CONTEXT_PROCESSORS =(
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.contrib.messages.context_processors.messages",
    "django.core.context_processors.request",
    "sekizai.context_processors.sekizai",
)


ROOT_URLCONF = 'sclubs.urls'


INSTALLED_APPS = (
    #'grappelli.dashboard',
    'grappelli',
    'django.contrib.auth',
    'django.contrib.comments',
    'django.contrib.contenttypes',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.sessions',
    'django.contrib.redirects',

    'sekizai',
    'south',
    'sorl.thumbnail',
    'grabbers',
    'ckeditor',
    'hitcount',
    #
    'sclubs.core',
    'sclubs.generic',
    'sclubs.accounts',
    'sclubs.news',
    'sclubs.seo',
    'sclubs.generic',
    'sclubs.articles',
    'sclubs.clubs',

    # django_cleanup should be placed after all apps
    'django_cleanup',
)



CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

SESSION_ENGINE = 'django.contrib.sessions.backends.signed_cookies'

AUTH_USER_MODEL = 'accounts.User'