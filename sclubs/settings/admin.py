__author__ = 'Evgeniy Tatarintsev'
import os.path

from .deploy import MEDIA_ROOT


CKEDITOR_UPLOAD_PATH = os.path.join(MEDIA_ROOT, 'uploads')

# For all settings look at http://docs.cksource.com/CKEditor_3.x/Developers_Guide/Setting_Configurations
CKEDITOR_CONFIGS = {
    'awesome_ckeditor': {
        'toolbar': 'Basic',
    },
    'default': {
        'toolbar': 'Full',
    },
    'bbcode':{
        'extraPlugins': 'bbcode',
    }
}