__author__ = 'Evgeniy Tatarintsev'
import os.path
from .deploy import MEDIA_ROOT


CKEDITOR_UPLOAD_PATH = os.path.join(MEDIA_ROOT, 'content_media')

PACKAGE_NAME_FILEBROWSER = 'filebrowser'

RATINGS_RANGE = (-1, 1)

#comments config
COMMENTS_DEFAULT_APPROVED = True
COMMENTS_UNAPPROVED_VISIBLE = False
COMMENTS_REMOVED_VISIBLE = False

# auth config
DEFAULT_USER_ID_LENGTH = 10
USER_ID_PREFIX = 'id'

HITCOUNT_KEEP_HIT_ACTIVE = {'days': 7}

ARTICLES_PER_PAGE = 10
READ_NOW_TIMEDELTA_SECONDS = 900
