$(document).ready(function(){

    //placeholder
    $('input[placeholder], textarea[placeholder]').placeholder();

    //окно выбора видов спорта    
    $('.type-sport-window input:checked').next('label').find('img').css('display', 'block'); 
    $('.type-sport-window input:checked').parent('li').addClass('li-active');

    $('.type-sport-window input').change(
        function(){
       //     $('.type-sport-window input:checked').next('label').find('img').css('display', 'block'); 
       //     $('.type-sport-window input:checked').parent('li').addClass('li-active');
            if ( $(this).prop('checked'))
            {
                $(this).next('label').find('img').css('display', 'block');   
                $(this).parent('li').addClass('li-active');
            }
            else
            {
                $(this).next('label').find('img').css('display', 'none');
                $(this).parent('li').removeClass('li-active');                
            }

        }

    );

    $('.type-sport-window li').hover(
        function(){
            $(this).addClass('li-hover');
        },
        function(){
            $(this).removeClass('li-hover');
        }

    );

    //позиционирование всплывающего окна с выбором видов спорта
    $('.wrap-article-title #sport-type-wrap').click(
            function(){
                a = $('.wrap-article-title').offset().top;
                b = $("#sport-type-wrap").position().top;
                c = $("#sport-type-wrap").height();
                d = $(".wrap-content").offset().top;
                dy = d-a-b-c;
 
                $('.type-sport-window').css('top', -dy)
                                        .css('border-top', '2px solid #235AC8');

            }
        );  

    //show sport-type window
    $('#sport-type-wrap').click(
        function(){
            $(this).toggleClass('sport-type-wrap-active');
            $('.type-sport-window').toggle();
            $(this).find('.border-none').toggle();

            return false;
        }
    );


   
    //checked all sports
    $('#choice-all').click(
        function(){
                if( $(this).html() == "Выбрать все")
                {
                   $('.type-sport-window').find('input:checkbox').attr("checked","checked");   
                   $('.type-sport-window input:checked').next('label').find('img').css('display', 'block'); 
                   $('.type-sport-window input:checked').parent('li').addClass('li-active'); 
                   $(this).html("Отменить");
                }
               else
               {
                   $('.type-sport-window input:checked').next('label').find('img').css('display', 'none'); 
                   $('.type-sport-window input:checked').parent('li').removeClass('li-active'); 
                   $('.type-sport-window').find('input:checkbox').removeAttr("checked");   
                   $(this).html("Выбрать все");

               }
               return false;
        }
    );            
    

    //check all sport in category
    $('.cat-sport input').change(
        function(){
             if ( $(this).prop('checked'))
             {
                $(this).closest('ul').find('input:checkbox').attr("checked","checked");   
                $(this).closest('ul').find('img').css('display', 'block'); 
                $(this).closest('ul').find('input:checkbox').parent('li').addClass('li-active'); 
             }
             else
            {
                $(this).closest('ul').find('img').css('display', 'none'); 
                $(this).closest('ul').find('input:checkbox').parent('li').removeClass('li-active'); 
                $(this).closest('ul').find('input:checkbox').removeAttr("checked");   

            }

        }
    );

    //processing click li
    $('.type-sport-window li').click(
        function(){
            $('#sport-type-2:first').trigger('change');            
        }

    );
    
    
    //add-events form
    $('.field-form').hover(
        function(){
            $(this).addClass('field-item-hover');
        },
        function(){
            $(this).removeClass('field-item-hover');
        }

    );

    $('.field-form').live('focusin',
        function(){
            $(this).addClass('field-item-focus');
        }

    ); 
    
    $('.field-form').live('focusout',
        function(){
            $(this).removeClass('field-item-focus');
        }
    );

/*    $('.field-form').one("focus",
        function(){
        }

    );
*/
    //wrap-doc-event
    $('#wrap-doc-event, .review').click(
        function(){
            $('#doc-event').trigger('click');
        }

    );

    $('#doc-event').change(
        function(){
            $('#wrap-doc-event').attr("value", $(this).val());
        }
    );

    //select template 
    $('.wrap-prew-template').hover(
        function(){
            $(this).addClass('wrap-prew-template-hover');
            $(this).find('a').show();
        },
        function(){
            $(this).removeClass('wrap-prew-template-hover');
            $(this).find('a').hide();
        }

    );


    $('.wrap-prew-template .select-template').click(
        function(){
            if ($(this).html() == "Выбрать")
            {
                $('.wrap-prew-template a').removeAttr("value");
                $('.wrap-prew-template a').html("Выбрать");
                $('.wrap-prew-template').removeClass('wrap-prew-template-active');
                $('#select-template').attr("value", $(this).attr('id'));
                $(this).closest('.wrap-prew-template').addClass('wrap-prew-template-active');
                $(this).html("Отменить");
            }
            else
            {
                $(this).closest('.wrap-prew-template').removeClass('wrap-prew-template-active');
                $(this).removeAttr("value");
                $(this).html("Выбрать");
                $('#select-template').attr("value", "");                
            }

            return false;

        }
    );

    //rew template list
    $('.rew-template').click(
        function(){
            if($('.template-list-curr').next().is('.template-list'))
            {
                $('.template-list-curr').next().addClass('template-list-curr');
                $(this).parent().find('.template-list-curr:first').removeClass('template-list-curr');
            }
            else
            {
                $('.template-list-curr').parent().find('.template-list:first').addClass('template-list-curr');
                $('.template-list-curr').parent().find('.template-list:last').removeClass('template-list-curr');
            }
            return false;
        }    
    );

    //media description photo

    $('.wrap-media-img').hover(
        function(){
            $(this).children('.media-desc').stop(true, true);
            $(this).children('.media-desc').fadeIn('slow');
        },
        function(){
            $(this).children('.media-desc').fadeOut('slow');
        }

    );


    // scroll to element
    function scroll_to_elem(elem,speed) {
        if(document.getElementById(elem))
        {
            var destination = $('#'+elem).offset().top;
            $("body").animate({scrollTop: destination}, speed);
        }
    }


    //answer
    $(".head-comments a").live('click',
        function(){
            id = $(this).attr("id").split("_");
        $('#post-comment-form #id_parent').attr("value", id[1] );
        $('#post-comment-form #id_comment').attr("value", $(this).parent().children("#name").html()+", ");
        $('#post-comment-form #id_comment').html($(this).parent().children("#name").html()+", ");
        $('#post-comment-form #id_name').focus();
        scroll_to_elem("id_name",500);
        return false;

        }
    );

    //ajax send comment
    var option = {
        type : "post", 
        dataType : "json",
        target : ".comments-block",
        success: function(json, statusText, xhr, $form){
                                    $('#post-comment-form').remove();
                                    $('.comments-block').append(json.form);
                                    if(!json.error)
                                    {
                                        if(!json.parent_id)
                                        {
                                            $('.comments-form').before(json.body_comm);
                                        }
                                        else
                                        {
                                            $('#'+json.parent_id).append(json.body_comm);
                                        }
                                        scroll_to_elem(json.comm_id ,500);
                                    }
                                    else
                                        $('.error_comments').html("Ошибка: \""+json.text_error+"\"!");
                          },

    };


    $('#post-comment-form').live("submit",
        function(){
            $(this).ajaxSubmit(option); 
            return false;
        }
    );



    //rew head rules 
    $('.l-rew a, .r-rew a').click(
        function(){
            var next = $(this).attr("href");
            $(this).closest('.content-nav').hide();
            $("#nav"+next).show();
            return false;
        }

    );

    //show page
    $('#nav1, #nav2, #nav3, #nav4, #nav5, #nav6, #nav7, #nav8, #nav9, #nav10, #nav11, #nav12, #nav13, #nav14, #nav15').hide();
    $('.content-nav-active').closest('.content-nav').show()


    //rand img in slider sidebar
    $('#r-img, #l-img').click(
        function(){
            $.get(
                "/gallery/random/",
                {
                },
                function(json){
                        $(".slider-img").css("background", "url("+json.img+") no-repeat");
                        $('#album-path').attr("href", json.url);
                },
                'json'
                );
            return false;            
        }

    );

    //start img
    $('#r-img').trigger("click");

    //redirect album
    $('.slider-img').click(
        function(){
            location.href = $('#album-path').attr('href');
        }
    );


    //sidebar nav tabs

    $('#last-update').click(
        function(){
            $.get(
                "/updates/last/",
                {}, 
                function(json){
                    $(".content-nav").remove();
                    $(".head-nav").after(json.block);
                    $('#event').parent().removeClass('nav-avtive');
                    $("#last-update").parent().addClass('nav-avtive');                    
                },
                "json"
            );
        }
    );

    $('#event').click(
        function(){
            $.get(
                "/events/ajax/future/",
                {}, 
                function(json){
                    $(".content-nav").remove();
                    $(".head-nav").after(json.block);
                    $('#last-update').parent().removeClass('nav-avtive');
                    $('#event').parent().addClass('nav-avtive');                    
                },
                "json"
            );
        }
    );

    $('#event a, #last-update a').click(
        function(){
            $(this).parent().trigger('click');
            return false;
        }
    );


    //index tabs
    $('.tab2 a').click(
        function(){
            if( !$('.container .article-popular').size())
                $.get(
                    "/articles/popular/",
                    {},
                    function(json){
                        $('.tab').after(json.content);
                    },
                    "json"
                    );
            $('.main-content, .pagination').hide();
            $('.article-popular').show();
            $('.tab1').removeClass('tab-content-active');
            $('.tab2').addClass('tab-content-active');
            return false;
        }

    );

    $('.tab1 a').click(
        function(){
            $('.article-popular').hide();
            $('.main-content, .pagination').show();
            $('.tab2').removeClass('tab-content-active');
            $('.tab1').addClass('tab-content-active');

            return false;
        }

    );


    //del query tab 
    $('#wrap-doc-event').focusout(
        function(){
            var index = $(this).index('.field-form');
            $('.field-form').eq(index+1).focus();
        }
    );





    //fix top-padding on internal pages
 /*   $('.wrap-article-title #select-city').click(
            function(){
                dx = $('.container').offset().top-271+31;
                rx = $('.wrap-article-title').width() - $('#sport-type-wrap').position().left;
                $('.select-city-window').css('top', -dx)
                                        .css('right', rx)
                                        .css('border-top', '2px solid #235AC8');

            }
        );
*/
    //show select-city window
    $('#select-city').click(
        function(){
            $(this).toggleClass('sport-type-wrap-active');
            $('.select-city-window').toggle();
            $(this).find('.border-none').toggle();
            return false;
        }
    );


    //fix top-padding on internal pages
    $('.wrap-article-title #select-city').click(
            function(){
                a = $('.wrap-article-title').offset().top;
                b = $("#select-city").position().top;
                c = $("#select-city").height();
                d = $(".wrap-content").offset().top;
                dy = d-a-b-c;
 
                rx = $('.wrap-article-title').width() - $('#sport-type-wrap').position().left;
                $('.select-city-window').css('top', -dy)
                                        .css('right', rx)
                                        .css('border-top', '2px solid #235AC8');

            }
        );    

    //send select all-city
    $('#all-city').click(
        function(){
            $(this).parent('form').trigger('submit');
            return false;
        }
    );

    //search from
    $('#search-button').click(
        function(){
            $(this).parent('form').trigger('submit');
            return false;
        }
    );


    //fix width h1
    var a = $('#select-city').position().left;
        b = a - 25;
    $('.article-title h1').css("width", b);

    var count = 3; //max item setting
    var statements = [
        "Дружба объединяет людей куда сильнее, чем любовь, а спорт сильнее чем дружба.",
        "Проникновение спортивных клубов в интернет - новая ступень в развитии спорта.",
        "Спорт становится средством воспитания тогда, когда это любимое занятие каждого."
    ];
    var item = 1;
    //slider in head
    $("#slider-right").click(
        function(){
            if (item == count) 
            {
                $("#item"+count).fadeOut();
                item = 1;
                $("#item"+1).fadeIn();
            }
            else
            {
                $("#item"+item).fadeOut();
                item++;
                $("#item"+item).fadeIn();
            }
            $(".slider-text .padding-text").html(statements[item-1]);
                return false;
        }
    );

    $("#slider-left").click(
        function(){
            if (item == 1) 
            {
                $("#item"+1).fadeOut();
                item = count;
                $("#item"+count).fadeIn();
            }
            else
            {
                $("#item"+item).fadeOut();
                item--;
                $("#item"+item).fadeIn();
            }
            $(".slider-text .padding-text").html(statements[item-1]);
                return false;
        }
    );    

    //FAQ
    $(".answer").hide();

    $(".question").click(
        function(){
            $(this).next(".answer").slideToggle();

            return false;
        }
    );

    //показываем капчу если при рендеринге страницы стоил галка
    if ( $("#no-register").prop('checked'))
    {
        $("#tr-captcha").show();
        $("#login-button").html("Зарегистрироваться");
    }



    //регистрация во всплывающем окне
    $("body").on("click", "#no-register",
        function(){
            if ( !$(this).prop('checked'))
            {
                $("#tr-captcha").hide();
                $("#login-button").html("Войти");
            }
            else
            {
                $("#tr-captcha").show();
                $("#login-button").html("Зарегистрироваться");
            }

        }

    );


    
    //показываем попап окно с формой логина
    $(".ajax-popup").click(
        function(){
            $.get(
                $(this).attr("href"),
                {}, 
                function(json){
                    $("body").append(json.content);
                    //позиционируем по центру попап окно 
                    var l = ($(".wrap-opacty").width() - $(".popup-login").width())/2
                    $(".popup-login").css("left", l+"px");                    
                },
                "json"
            );

            return false;
        }

    );

    //срываем окно логина
    $("body").on("click", ".wrap-opacty",
        function(){
            $(".popup-login").remove();
            $(this).remove();

        }
        
    );


    //появление кнопки добавить в меню юзера
    /*
    $(".text-nav").hover(
        function(){
            $(this).children(".add-item-nav").stop(true,true);
            $(this).children(".add-item-nav").fadeIn("slow");
        },
        function(){
            $(this).children(".add-item-nav").fadeOut("slow");
        }

    );
    */
});
