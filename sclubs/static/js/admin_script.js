$(document).ready(function(){
  

 //view advanced settigs
 $('.adv').click(
    function(){
        $('.adv-admin-article').toggle();
    }
 );   

 //tinymce settings   
    tinyMCE.init({
                // General options
		mode : "textareas",
        language:"ru",
		theme : "advanced",
                plugins : "safari,pagebreak,style,layer,table,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,",
                extended_valid_elements : 'script[type|src],iframe[src|style|width|height|scrolling|marginwidth|marginheight|frameborder],div[*],p[*],object[width|height|classid|codebase|embed|param],param[name|value],embed[param|src|type|width|height|flashvars|wmode]',
		media_strict: false,
                theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
                theme_advanced_buttons2 : "bullist,|,outdent,indent,|,undo,redo,|,table,|,visualchars,|,preview,|,forecolor,backcolor,|,hr,removeformat|,link,unlink,anchor,image,cleanup,code",
                theme_advanced_toolbar_location : "top",
                theme_advanced_toolbar_align : "center",
                theme_advanced_statusbar_location : "bottom",
                theme_advanced_resizing : true,
		relative_urls : "false",
		remove_script_host : false,
		convert_urls : false
}); 
   
   
  //ajax admin sites 
  
    $('td > input:checkbox').click(
        function(){
                $.post(
                    "/admin/ajax/",
                    {
                            "type": $(this).attr("name"),
                            "id":$(this).attr("value"),
                            "csrfmiddlewaretoken":$('input:[name=csrfmiddlewaretoken]').attr("value"),

                    },
                    function(json){
                        //alert("req="+json.value);
                        if(json.value)
							{
								//alert(1); проверить галку
								$('input[value='+json.id+'][name='+json.type+']').attr('checked');
								$('input[value='+json.id+'][name='+json.type+']').next("label").children("img").attr("src", "/media/img/yes-admin.gif");
							}
                        else
							{
								$('input[value='+json.id+'][name='+json.type+']').removeAttr('checked');
								$('input[value='+json.id+'][name='+json.type+']').next("label").children("img").attr("src", "/media/img/no-admin.gif");
							}
                    },
                    'json'
                    
                );
        }
    );  
	
	$('.ifcaton').click(
		function(){
			$('.ifart').hide();
			$('.ifcat').show();
			return false;
		}
				
	);
	

	$('.ifarton').click(
		function(){
			$('.ifcat').hide();
			$('.ifart').show();
			return false;
		}
		
		
	);
	
	//admin gallery - show edit button
	$('.td-wrap').hover(
		function(){
			$(this).children('#album-preview').next('a').fadeIn('slow');
		},
		function(){
			$(this).children('#album-preview').next('a').fadeOut();
		}
	);


	//admin gallery - show del button
	$('.td-wrap').hover(
		function(){
			$(this).children('.image-title').next('a').fadeIn('slow');
		},
		function(){
			$(this).children('.image-title').next('a').fadeOut();
		}
	);	

	//admin gallery del image ajax
	$('.image-del').click(
		function(){
			//alert($(this).parent('.td-wrap').children('img').attr('id'));
			$.post(
			"/admin/image-del/",
			{
				"id":$(this).parent().children('img').attr('id'),
				"csrfmiddlewaretoken":$('input:[name=csrfmiddlewaretoken]').attr("value")
			
			},
			function(json){
				if(json.value){
					$('#'+json.id+'').parent().html('<div class="image-notify">Удалено</div>');
				}
				else{
					$('#'+json.id+'').parent().html('<div class="image-notify">Ошибка сервера.<br>Перезагрузите страницу.</div>');
				}
			},
			'json'
			);
			return false;
		}
	
	);


	//GLOBAL VARS
	var id_td, content_td;
	//admin schedule show input
	$('.schedule td').dblclick(
		function(){
			var id = $(this).attr('id');
			var content = $(this).html();
  			var inp = $(this).find('*').is('input');		//if has input - true

  			if ( inp == false )
			{
	  			$('#'+id_td+'').html(content_td);
	  			id_td = $(this).attr('id');	
	  			content_td = content;
				$(this).html('');
				$(this).parent().find('input').remove();
				$(this).append("<input class='time' type='text' value="+content+">");
				$(this).children('input').focus();
			}
		}	
	);

	//admin schedule ajax
	$('.schedule td input').live('keypress',
		function(key){
			if(key.which == 13)
			{
				$.post(
					"/admin/schedule/add_time/",
					{
						"id":$(this).parent('td').attr('id'),
						"time":$(this).val(),
						"csrfmiddlewaretoken":$('input:[name=csrfmiddlewaretoken]').attr("value")		

					},
					function(json){
						if(json.value){
							$('#'+json.id+'').append(''+json.time+'');
							content_td = json.time;
						}
						else{
							$('#'+json.id+'').append(''+content_td+'');
						}
					},
					"json"
					);
				$(this).remove();
			}
			 //mask for time 
 			$('.time').mask("99:99-99:99");
		}
	);


	//admin schedule - show del button
	$('.schedule').hover(
		function(){
			$(this).find('#group-edit:first').fadeIn('slow');
		},
		function(){
			$(this).find('#group-edit:first').fadeOut();
		}
	);		

});

